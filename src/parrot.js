export class Parrot {
    get BASE_SPEED() {
        return 12;
    }

    getSpeed() {
        throw new Error("Should be unreachable");
    }
}

export class EuropeanParrot extends Parrot {
    getSpeed() {
        return this.BASE_SPEED;
    }
}

export class AfricanParrot extends Parrot {
    constructor(numberOfCoconuts) {
        super();
        this.numberOfCoconuts = numberOfCoconuts;
    }

    getSpeed() {
        return Math.max(0, this.BASE_SPEED - this.LOAD_FACTOR * this.numberOfCoconuts);
    }

    get LOAD_FACTOR() {
        return 9;
    }
}

export class NorwegianBlueParrot extends Parrot {
    constructor(voltage, isNailed) {
        super();
        this.voltage = voltage;
        this.isNailed = isNailed;
    }

    getSpeed() {
        return (this.isNailed) ? 0 : this.#getBaseSpeedWithVoltage(this.voltage);
    }

    #getBaseSpeedWithVoltage(voltage) {
        return Math.min(24, voltage * this.BASE_SPEED);
    }
}
