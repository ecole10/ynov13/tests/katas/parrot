import {AfricanParrot, EuropeanParrot, NorwegianBlueParrot} from '../parrot';

describe("EuropeanParrot", () => {
    test("get speed of european parrot", () => {
        // Arrange
        const europeanParrot = new EuropeanParrot();
        // Act
        const result = europeanParrot.getSpeed();
        // Assert
        expect(result).toBe(12);
    });
});

describe.each([
    [0, 12],
    [1, 3],
    [2, 0],
])('AfricanParrot', (coconutNumber, expectedResult) => {
    test(`get speed of african parrot with $coconutNumber coconut`, () => {
        // Arrange
        const africanParrot = new AfricanParrot(coconutNumber);
        // Act
        const result = africanParrot.getSpeed();
        // Assert
        expect(result).toBe(expectedResult);
    });
});

describe("NorwegianBlueParrot", () => {
    test("get speed norwegian blue parrot nailed", () => {
        // Arrange
        const norwegianBlueParrot = new NorwegianBlueParrot(1.5, true);
        // Act
        const result = norwegianBlueParrot.getSpeed();
        // Assert
        expect(result).toBe(0);
    });

    test("get speed norwegian blue parrot not nailed", () => {
        // Arrange
        const norwegianBlueParrot = new NorwegianBlueParrot(1.5, false);
        // Act
        const result = norwegianBlueParrot.getSpeed();
        // Assert
        expect(result).toBe(18);
    });

    test("get speed norwegian blue parrot not nailed high voltage", () => {
        // Arrange
        const norwegianBlueParrot = new NorwegianBlueParrot(4, false);
        // Act
        const result = norwegianBlueParrot.getSpeed();
        // Assert
        expect(result).toBe(24);
    });
});
